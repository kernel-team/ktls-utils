ktls-utils for Debian
---------------------

ktls-utils currently includes the tlshd daemon, which is used by
"in-kernel TLS consumers" to establish TLS connections including
certificate validation.  The current in-kernel users of TLS are the
NFS server (from Linux 6.4) and the NFS client (from Linux 6.5).

WARNING: tlshd is currently experimental and probably does not
implement all of the certificate validation that you want.

To enable TLS on an NFS server:

1. Create a private key and certificate for the server.  The signing CA
   needs to be trusted on the *server*, and normally also on the
   client.  As with any server certificate, the names in this
   certificate should include all the domain names that clients will
   use to connect to the server.
2. Put the filenames of the private key and certificate in the
   [authenticate.server] section of /etc/tlshd.conf.  These cannot be
   symlinks.
3. Add the "xprtsec=..." option to the relevant lines in /etc/exports.
   Normally you would use either "xprtsec=mtls" to require TLS and
   trusted client certificates, or "xprtsec=mtls:tls:none" to make TLS
   optional.  See exports(5) for the details of this option.

To enable TLS on an NFS client:

1. If the server requires client certificates:
   1. Create a private key and certificate for the client.  The signing
      CA needs to be trusted on both the client and the server.  The
      name(s) and purposes in this certificate are currently ignored.
   2. Put the filenames of the private key and certificate in the
      [authenticate.client] section of /etc/tlshd.conf.  These cannot be
      symlinks.
2. Add the "xprtsec=..." option to the relevant lines in /etc/fstab.
   Normally you would use either "xprtsec=mtls" to require TLS and a
   trusted server certificate, or "xprtsec=mtls:tls:none" to make TLS
   optional.  See nfs(5) for the details of this option.
3. If the client should verify the server's certificate, use a domain
   name for the server in /etc/fstab, not an IP address.

 -- Ben Hutchings <benh@debian.org>, Mon, 24 Jul 2023 01:53:26 +0200
